import React, {Component} from 'react';
import firebase from 'firebase';
import _ from 'lodash';
import Header from './components/Header';
import Grid from './components/Grid';
import Form from './components/Form';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: [],
      name: 'Edu',
      currentTitle: '',
      currentDetails: '',
    };
  }

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "<own api key>",
      authDomain: "edu-notepad.firebaseapp.com",
      databaseURL: "https://edu-notepad.firebaseio.com",
      projectId: "edu-notepad",
      storageBucket: "",
      messagingSenderId: "<own sender id>"
    });

    firebase.database().ref('/notes')
      .on('value', snapshot => {
        const fbstore = snapshot.val();

        const store = _.map(fbstore, (value, id) => ({
          id,
          title: value.title,
          details: value.details,
        }));

        this.setState({
          notes: store,
        });
      });
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = {
      title: this.state.currentTitle,
      details: this.state.currentDetails,
    };

    firebase.database().ref('/notes').push(data, response => response);

    this.setState({
      currentTitle: '',
      currentDetails: '',
    });
  }

  deleteNote(id) {
    firebase.database().ref(`/notes/${id}`).remove();
  }

  render() {
    return (
      <div className="App">
        <Header name={this.state.name}/>
        <Form
          currentTitle={this.state.currentTitle}
          currentDetails={this.state.currentDetails}
          handleChange={this.handleChange.bind(this)}
          handleSubmit={this.handleSubmit.bind(this)}
        />
        <Grid notes={this.state.notes} deleteNote={this.deleteNote.bind(this)}/>
      </div>
    );
  }
}

export default App;
